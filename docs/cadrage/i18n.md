# Internationalisation - i18n
<a href="https://weblate.framasoft.org/engage/funky-framadate/">
<img src="https://weblate.framasoft.org/widgets/funky-framadate/-/funky-framadate-front/svg-badge.svg" alt="État de la traduction" />
</a>

## Dans les templates HTML

Toutes les chaînes de texte doivent être disponible en minimum deux langues: Français et Anglais.

Pour l'ajout de nouvelles traductions voir [la documentations des traductions.md](../traductions.md)
La documentation a été pensée pour être compréhensible en premier lieu par des personnes francophones, le projet étant issu de Framasoft et de personnes uniquement Francophones, nous avons jugé que c'était le moyen le plus efficace pour le faire grandir.

Voir les fichiers dans le dossier **src/assets/i18n**
[EN.json](/src/assets/i18n/en.json) et [FR.json](/src/assets/i18n/fr.json)

La traduction se base sur un système de clés-valeur dans des fichiers JSON.
Les clés sont entrées dans les templates html, et c'est la config d'Angular qui les traduit selon la langue demandée par le visiteur du site.

Utilisez des sous groupes dans vos traductions afin de mieux segmenter les chaines de caractère par page et selon leur sens.
Faites en sorte de réutiliser au maximum les phrases identiques.

## Traduction de la documentation mkdocs

Pour traduire cette documentation (les fichiers markdwon dans le dossier /docs/) qui génère un mini site avec mkdocs avec la commande de build, il faut faire des fichiers avec un nom de langue tels que index.fr.md et index.en.md.
Plus d'infos: https://www.mkdocs.org/user-guide/configuration/#lang
