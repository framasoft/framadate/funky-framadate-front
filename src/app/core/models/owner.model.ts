import { UserRole } from '../enums/user-role.enum';
import { Poll } from './poll.model';

export class Owner {
	constructor(
		public pseudo: string = '',
		public email: string = '',
		public polls: Poll[] = [],
		public role: UserRole = UserRole.ADMIN,
		public modifier_token: string = '',
		public created_at: string = new Date().toISOString()
	) {}
}
