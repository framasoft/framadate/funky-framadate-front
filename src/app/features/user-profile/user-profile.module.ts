import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../shared/shared.module';
import { UserPollsComponent } from './user-polls/user-polls.component';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { AppModule } from '../../app.module';

@NgModule({
	declarations: [UserPollsComponent],
	imports: [
		CommonModule,
		UserProfileRoutingModule,
		SharedModule,
		TranslateModule.forChild({ extend: true }),
		AppModule,
	],
})
export class UserProfileModule {}
